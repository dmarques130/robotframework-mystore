*** Settings ***

Library     SeleniumLibrary
Library     FakerLibrary

*** Variables ***

${BROWSER}                              Chrome
${URL}                                  http://automationpractice.com
${EMAIL_FAKE}                           FakerLibrary.Free Email
&{PESSOA}                               firstname=Pietra  
...                                     lastname=Elza Melo  
...                                     days=2  
...                                     months=5  
...                                     years=1992    
...                                     address=Rua B
...                                     city=Denver
...                                     state=Colorado
...                                     country=United States
...                                     postcode=00000
...                                     phonemobile=11984488747


*** Keywords ***

Abrir Navegador

    Open Browser                        about:blank                                ${BROWSER}               options=add_experimental_option('excludeSwitches', ['enable-logging'])

Fechar Navegador

    Close Browser
    
Dado que estou na página home do site

    Go To                               ${URL}
    Title Should Be                     My Store

Quando eu pesquisar pelo produto "${PRODUTO}"

    Input Text                          id=search_query_top                        ${PRODUTO}
    Click Element                       name=submit_search

Então o produto "${PRODUTO}" deve ser listado na página de resultado da busca

    Wait Until Element Is Visible       css=#center_column h1.product-listing
    Title Should Be                     Search - My Store
    Page Should Contain Image           xpath=//*[@id="center_column"]//img[@title="Blouse"]
    Page Should Contain Link            xpath=//*[@id="center_column"]//a[@class="product-name"]

Quando eu registrar uma nova conta

    ${EMAILFAKE}                        FakerLibrary.Email

    Click Element                       css=#header a.login
    Title Should Be                     Login - My Store
    Input Text                          id=email_create                            ${EMAILFAKE}
    Click Element                       id=SubmitCreate 
    Wait Until Element Is Visible       id=account-creation_form 
    Element Should Be Visible           id=account-creation_form
    Input Text                          id=customer_firstname                      ${PESSOA.firstname}
    Input Text                          id=customer_lastname                       ${PESSOA.lastname}
    Textfield Value Should Be           id=email                                   ${EMAILFAKE} 
    Input Text                          id=passwd                                  123123  

    Select From List By Value           id=days                                    ${PESSOA.days}
    Select From List By Value           id=months                                  ${PESSOA.months}
    Select From List By Value           id=years                                   ${PESSOA.years}

    ${DAYS}=                            Get Selected List Value                    id=days
    ${MONTHS}=                          Get Selected List Value                    id=months
    ${YEARS}=                           Get Selected List Value                    id=years

    Should Be Equal                     ${DAYS}                                    ${PESSOA.days}
    Should Be Equal                     ${MONTHS}                                  ${PESSOA.months}
    Should Be Equal                     ${YEARS}                                   ${PESSOA.years}

    Textfield Value Should Be           css=div.account_creation #firstname        ${PESSOA.firstname}
    Textfield Value Should Be           css=div.account_creation #lastname         ${PESSOA.lastname}

    Input Text                          id=address1                                ${PESSOA.address}
    Input Text                          id=city                                    ${PESSOA.city}

    Select From List By Label           id=id_state                                ${PESSOA.state}
    Select From List By Label           id=id_country                              ${PESSOA.country}

    ${STATE}=                           Get Selected List Label                    id=id_state
    ${COUNTRY}=                         Get Selected List Label                    id=id_country

    Should Be Equal                     ${STATE}                                   ${PESSOA.state}
    Should Be Equal                     ${COUNTRY}                                 ${PESSOA.country}

    Input Text                          id=postcode                                ${PESSOA.postcode}
    Input Text                          id=phone_mobile                            ${PESSOA.phonemobile}
    Input Text                          id=alias                                   ${PESSOA.address}

    Click Element                       id=submitAccount

Então a página deve exibir a mensagem "${MENSAGEM}"

    Wait Until Element Is Visible       css=#center_column p.alert-warning
    Title Should Be                     Search - My Store
    Element Text Should Be              css=#center_column p.alert-warning         ${MENSAGEM}



Então devo visualizar a página de gerenciamento da conta

    Title Should Be                     My account - My Store
    Element Text Should Be              css=div.header_user_info a.account span    ${PESSOA.firstname} ${PESSOA.lastname}        








