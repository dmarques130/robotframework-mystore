*** Settings ***

Resource            ../resources/Resource.robot
Test Setup          Abrir Navegador
Test Teardown       Fechar Navegador

*** Test Cases ***

Cenario 01: Registrar nova conta
    Dado que estou na página home do site
    Quando eu registrar uma nova conta
    Então devo visualizar a página de gerenciamento da conta