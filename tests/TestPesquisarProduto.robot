*** Settings ***

Resource            ../resources/Resource.robot
Test Setup          Abrir Navegador
Test Teardown       Fechar Navegador

*** Test Cases ***

Cenario 01: Pesquisar produto existente
    Dado que estou na página home do site
    Quando eu pesquisar pelo produto "Blouse"
    Então o produto "Bluse" deve ser listado na página de resultado da busca

Cenario 02: Pesquisar produto não existente
    Dado que estou na página home do site
    Quando eu pesquisar pelo produto "ItemNãoExistente"
    Então a página deve exibir a mensagem "No results were found for your search "ItemNãoExistente""

    



